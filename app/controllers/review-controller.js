const mysqlConnection = require("../config/mysql");

let create = (req, res) => {
  let insert = `INSERT INTO ${process.env.TABLE_REVIEW} SET ?`;
  let query = mysqlConnection.format(insert, req.body);
  mysqlConnection.query(query, (error, result) => {
      if (error) throw error;
      res.json([{ respuesta: `Reseña agregada con exito` }]);
    }
  );
};

let getAll = (req, res) => {
  mysqlConnection.query(`SELECT * FROM ${process.env.TABLE_REVIEW}`, (err, rows) => {
    if (!err) {
      return res.status(200).json(rows);
    } else {
      console.log(err);
    }
  });
};

let update = (req, res) => {
  const id = req.params.id;
  mysqlConnection.query(
    `UPDATE ${process.env.TABLE_REVIEW} SET ? WHERE id = ?`,
    [req.body, id],
    (error, result) => {
      if (error) throw error;
      res.json([{ status: `Reseña actualizada exitosamente.` }]);
    }
  );
};

module.exports = {
  create,
  getAll,
  update,
};
