const express = require("express");
const reviewController = require("../controllers/review-controller");
const router = express.Router();

// GET all review
router.get('', reviewController.getAll);

// CREATE review
router.post("/add", reviewController.create);

// UPDATE review
router.put("/update/:id", reviewController.update);

module.exports = router;