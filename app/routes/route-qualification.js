const express = require("express");
const qualificationController = require("../controllers/qualification-controller");
const router = express.Router();

// GET all review
router.get('', qualificationController.getAll);

// CREATE review
router.post("/add", qualificationController.create);

// UPDATE review
router.get("/:id", qualificationController.getById);


module.exports = router;
