require('dotenv').config();
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const consultBooks = require('./routes/consultBooks'); 
const review = require('./routes/route-review');
const qualification = require('./routes/route-qualification'); 

const app = express()
  .use(cors({ credentials: true, origin: "http://localhost:4200" }))
  .use(bodyParser.urlencoded({ extended: false }))
  .use(bodyParser.json());
  
app.use('/consultBooks', consultBooks);
app.use('/review', review);
app.use('/qualification', qualification);

module.exports = app;