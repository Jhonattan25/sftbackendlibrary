const mysqlConnection = require("../config/mysql");

let create = (req, res) => {
  let insert = `INSERT INTO ${process.env.TABLE_QUALIFICATION} SET ?`;
  let query = mysqlConnection.format(insert, req.body);
  mysqlConnection.query(query, (error, result) => {
      if (error) throw error;
      res.json([{ respuesta: `Calificaion agregada con exito` }]);
    }
  );
};

let getAll = (req, res) => {
  mysqlConnection.query(
    `SELECT l.autor, l.titulo, u.nombre, c.numCalificacion 
        FROM ${process.env.TABLE_QUALIFICATION} AS c 
        INNER JOIN ${process.env.TABLE_BOOK} AS l ON l.ISBN = c.libro 
        INNER JOIN ${process.env.TABLE_USER} AS u  ON c.usuario = u.id`,
    (err, rows) => {
      if (!err) {
        return res.status(200).json(rows);
      } else {
        console.log(err);
      }
    }
  );
};

let getById = (req, res) => {
  const id = req.params.id;
  mysqlConnection.query(
    `SELECT l.autor, l.titulo, u.nombre, c.numCalificacion 
     FROM ${process.env.TABLE_QUALIFICATION} AS c 
     INNER JOIN ${process.env.TABLE_BOOK} AS l ON l.ISBN = c.libro 
     INNER JOIN ${process.env.TABLE_USER} AS u  ON c.usuario = u.id 
     WHERE id = ${id}`,
    (error, rows) => {
      if (!error) {
        return res.status(200).json(rows);
      } else {
        console.log(error);
      }
    }
  );
};

module.exports = {
  create,
  getAll,
  getById,
};
