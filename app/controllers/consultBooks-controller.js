const mysqlConnection = require("../config/mysql");

let getAll = (req, res) => {
  mysqlConnection.query(`SELECT * FROM ${process.env.TABLE_BOOK}`, (err, rows) => {
    if (!err) {
      return res.status(200).json(rows);
    } else {
      console.log(err);
    }
  });
};

let getByParam = (req, res) => {
  const { param } = req.params;
  let select = `SELECT * FROM ${process.env.TABLE_BOOK} WHERE autor LIKE ? OR titulo LIKE ?`;
  let query = mysqlConnection.format(select, [`%${param}%`, `%${param}%`]);
  mysqlConnection.query(query, (err, rows, fields) => {
    if (!err) {
      return res.status(200).json(rows);
    } else {
      console.log(err);
    }
  });
};

let getByParams = (req, res) => {
  const { isb , author, title} = req.params;

  let select = `SELECT * FROM ${process.env.TABLE_BOOK} WHERE ISBN LIKE ? OR autor LIKE ? OR titulo LIKE ?`;
  let query = mysqlConnection.format(select, [`%${isb}%`, `%${author}%`, `%${title}%`]);
  mysqlConnection.query(query, (err, rows, fields) => {
    if (!err) {
      return res.status(200).json(rows);
    } else {
      console.log(err);
    }
  });
};

module.exports = {
  getAll,
  getByParam,
  getByParams
};
