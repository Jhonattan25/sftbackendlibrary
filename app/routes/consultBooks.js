const express = require("express");
const consultBooksController = require('../controllers/consultBooks-controller');
const router = express.Router();

// GET all Books
router.get('/', consultBooksController.getAll);

// GET Books By parameter
router.get("/:param", consultBooksController.getByParam);

// GET Books By ISB, author and title
router.get("/:isb/:author/:title", consultBooksController.getByParams);

module.exports = router;
